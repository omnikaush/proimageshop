package edu.uchicago.gerber.base_code.pro_imageshop;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


public class ImageShopMain extends Application {

     //public static Stage stage;
    //changing things here again

    @Override
    public void start(Stage stage) {

       // this.stage = stage;

        //Stage
        //Scene
        //Root

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/imageshop.fxml"));
        } catch (IOException e) { e.printStackTrace(); }
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/first.css");
        stage.setTitle("ImageShop");
        stage.setScene(scene);
        stage.setOnHidden(e -> Platform.exit());
        stage.show();

        Cc.getInstance().setMainStage(stage);

        //open up toolbar
        Stage dialogStage = new Stage();
        Parent root2 = null;
        try {
            root2 = FXMLLoader.load(getClass().getResource("/fxml/toolbar.fxml"));
            Scene scene2 = new Scene(root2, 200, 400);
            scene2.getStylesheets().add("/styles/first.css");
            dialogStage.setTitle("Toolbar");
            dialogStage.setScene(scene2);
            Cc.getInstance().setmToolbarStage(dialogStage);
            dialogStage.show();
            dialogStage.setX(stage.getX()-210);
            dialogStage.setY(stage.getY()+140);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}