package edu.uchicago.gerber.base_code.pro_imageshop;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.w3c.dom.css.Rect;

import javax.imageio.ImageIO;

/**
 * SampleGeneric Skeleton for 'imageshop.fxml' Controller Class
 */


public class ImageShopController implements Initializable {

    //private Desktop desktop = Desktop.getDesktop();
    private ToggleGroup mToggleGroup = new ToggleGroup();

    public enum Pen {
        CIR, SQR, FIL, OTHER;
    }

    public enum FilterStyle {
        SAT, DRK, OTHER;
    }



    private int penSize = 50;
    private Pen penStyle = Pen.CIR;
    private FilterStyle mFilterStyle = FilterStyle.DRK;

    @FXML // fx:id="imgView"
    private ImageView imgView; // Value injected by FXMLLoader

//    @FXML // ResourceBundle that was given to the FXMLLoader
//    private ResourceBundle resources;

    // for mouse clicks
    private double xPos, yPos, hPos, wPos;

    private Color mColor = Color.WHITE;

    ArrayList<Shape> removeShapes = new ArrayList<>(1000);


    //http://java-buddy.blogspot.com/2013/01/use-javafx-filechooser-to-open-image.html
    @FXML
    void mnuOpenAction(ActionEvent event) {

        Cc.getInstance().clearSelectionArea();
        Cc.getInstance().setImgView(this.imgView);


        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);

        //Show open file dialog
        File file = fileChooser.showOpenDialog(null);
        //openFile(file);


        try {
            BufferedImage bufferedImage = ImageIO.read(file);


            // imgView.setImage(Cc.getInstance().getImg());

            Cc.getInstance().setImageAndRefreshView(SwingFXUtils.toFXImage(bufferedImage, null));

        } catch (IOException ex) {
            Logger.getLogger(ImageShopController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Cc.getInstance().setLastfile(Cc.getInstance().getCurrentfile());
        Cc.getInstance().setCurrentfile(file);

        //refresh image so I don't get buggy filter behavior
        snapshotImage();

    }


//    @FXML
//    private ComboBox<String> cboSome;

    @FXML
    private ToggleButton tgbSquare;


    @FXML
    private ToggleButton tgbCircle;


    @FXML
    private ColorPicker cpkColor;



    @FXML
    private Slider sldSize;

//    @FXML
//    private ToggleButton tgbFilter;

    @FXML
    void mnuAbout(ActionEvent event) {
        Stage dialogStage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/about.fxml"));
            Scene scene = new Scene(root);
            dialogStage.setTitle("About");
            dialogStage.setScene(scene);
            //set the stage so that I can close it later.
            //Cc.getInstance().setSaturationStage(dialogStage);
            dialogStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    void mnuNew(ActionEvent event) {
        Cc.getInstance().close();
        //load up blank image
        Cc.getInstance().setImgView(this.imgView);
        //File file = new File("C:\\dev\\java\\javasummer2016\\proImageShop\\src\\main\\resources\\images\\blank.png");
        File file = new File(System.getProperty("user.dir")+"\\src\\main\\resources\\images\\blank.png");
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Cc.getInstance().setImageAndRefreshView(SwingFXUtils.toFXImage(bufferedImage, null));
        } catch (IOException ex) {
            Logger.getLogger(ImageShopController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Cc.getInstance().setLastfile(Cc.getInstance().getCurrentfile());
        Cc.getInstance().setCurrentfile(file);
        //refresh image so I don't get buggy filter behavior
        snapshotImage();

    }

    @FXML
    void mnuReOpenLast(ActionEvent event) {

        Cc.getInstance().clearSelectionArea();
        File file = Cc.getInstance().getLastfile();
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Cc.getInstance().setImageAndRefreshView(SwingFXUtils.toFXImage(bufferedImage, null));
        } catch (IOException ex) {
            Logger.getLogger(ImageShopController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Cc.getInstance().setLastfile(Cc.getInstance().getCurrentfile());
        Cc.getInstance().setCurrentfile(file);
    }

    @FXML
    private AnchorPane ancRoot;


    @FXML
    void mnuSaveAction(ActionEvent event) {

    }



    @FXML
    void mnuSaveAsAction(ActionEvent event) {

    }

    @FXML
    void mnuQuitAction(ActionEvent event) {


        System.exit(0);


    }

    @FXML
    void mnuCloseAction(ActionEvent event) {
        Cc.getInstance().close();
        File file = new File(System.getProperty("user.dir")+"\\src\\main\\resources\\images\\blank.png");
        Cc.getInstance().setLastfile(Cc.getInstance().getCurrentfile());
        Cc.getInstance().setCurrentfile(file);

    }

    @FXML
    void mnuGrayscale(ActionEvent event) {


        if (Cc.getInstance().getImg() == null)
            return;

        //get selection area if there is one active
        Rectangle select = MiscCode.rectangleCopy(Cc.getInstance().getSelectionRectangle());
        Cc.getInstance().clearSelectionArea();
        //make sure that we set the image view first, so we can roll back and do other operations to it.
        Cc.getInstance().setImgView(this.imgView);

        Image greyImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                select,
                Color::grayscale);
        Cc.getInstance().setImageAndRefreshView(greyImage);


    }

    @FXML
    void mnuSaturate(ActionEvent event) {


        Cc.getInstance().setImgView(this.imgView);


        Stage dialogStage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/saturation.fxml"));
            Scene scene = new Scene(root);
            dialogStage.setTitle("Saturation");
            dialogStage.setScene(scene);
            //set the stage so that I can close it later.
            Cc.getInstance().setSaturationStage(dialogStage);
            dialogStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @FXML
    void mnuDesaturate(ActionEvent event) {


        if (Cc.getInstance().getImg() == null)
            return;

        //get selection area if there is one active
        Rectangle select = MiscCode.rectangleCopy(Cc.getInstance().getSelectionRectangle());
        Cc.getInstance().clearSelectionArea();
        //make sure that we set the image view first, so we can roll back and do other operations to it.
        Cc.getInstance().setImgView(this.imgView);
        Image image = Cc.transform(Cc.getInstance().getImg(),
                select,
                (Color c) -> c.deriveColor(0, 0.5, 1.0, 1.0));
        Cc.getInstance().setImageAndRefreshView(image);


    }

    @FXML
    void mnuDarken(ActionEvent event) {


        if (Cc.getInstance().getImg() == null)
            return;

        //get selection area if there is one active
        Rectangle select = MiscCode.rectangleCopy(Cc.getInstance().getSelectionRectangle());
        Cc.getInstance().clearSelectionArea();
        //make sure that we set the image view first, so we can roll back and do other operations to it.
        Cc.getInstance().setImgView(this.imgView);
        Image image = Cc.transform(Cc.getInstance().getImg(),
                select,
                (Color c) -> c.deriveColor(0, 1.0, 0.5, 1.0));
        Cc.getInstance().setImageAndRefreshView(image);


    }

    @FXML
    void mnuLighten(ActionEvent event) {


        if (Cc.getInstance().getImg() == null)
            return;

        //get selection area if there is one active
        Rectangle select = MiscCode.rectangleCopy(Cc.getInstance().getSelectionRectangle());
        Cc.getInstance().clearSelectionArea();
        //make sure that we set the image view first, so we can roll back and do other operations to it.
        Cc.getInstance().setImgView(this.imgView);
        Image image = Cc.transform(Cc.getInstance().getImg(),
                select,
                (Color c) -> c.deriveColor(0, 1.0, 2.0, 1.0));
        Cc.getInstance().setImageAndRefreshView(image);


    }

    @FXML
    void mnuInvert(ActionEvent event) {


        if (Cc.getInstance().getImg() == null)
            return;

        //get selection area if there is one active
        Rectangle select = MiscCode.rectangleCopy(Cc.getInstance().getSelectionRectangle());
        Cc.getInstance().clearSelectionArea();
        //make sure that we set the image view first, so we can roll back and do other operations to it.
        Cc.getInstance().setImgView(this.imgView);
        Image image = Cc.transform(Cc.getInstance().getImg(),
                select,
                Color::invert);
        Cc.getInstance().setImageAndRefreshView(image);


    }

    //I forgot where I borrowed this from...
    @FXML
    void mnuSepia(ActionEvent event) {


        if (Cc.getInstance().getImg() == null)
            return;

        //get selection area if there is one active
        Rectangle select = MiscCode.rectangleCopy(Cc.getInstance().getSelectionRectangle());
        Cc.getInstance().clearSelectionArea();
        //make sure that we set the image view first, so we can roll back and do other operations to it.
        Cc.getInstance().setImgView(this.imgView);
        Image image = Cc.transform(Cc.getInstance().getImg(),
                select,
                (Color c) -> {
                    double newred = c.getRed()*.393 + c.getGreen()*.769 + c.getBlue()*.189;
                    double newgreen = c.getRed()*.349 + c.getGreen()*.686 + c.getBlue()*.168;
                    double newblue = c.getRed()*.272 + c.getGreen()*.534 + c.getBlue()*.131;

                    int endred = (int) (newred > 1 ? 255 : newred*255);
                    int endgreen = (int) (newgreen > 1 ? 255 : newgreen*255);
                    int endblue = (int) (newblue > 1 ? 255 : newblue*255);

                    return Color.rgb(endred,endgreen,endblue);
                });
        Cc.getInstance().setImageAndRefreshView(image);


    }

    @FXML
    void mnuViolet(ActionEvent event) {


        if (Cc.getInstance().getImg() == null)
            return;

        //get selection area if there is one active
        Rectangle select = MiscCode.rectangleCopy(Cc.getInstance().getSelectionRectangle());
        Cc.getInstance().clearSelectionArea();
        //make sure that we set the image view first, so we can roll back and do other operations to it.
        Cc.getInstance().setImgView(this.imgView);
        Image image = Cc.transform(Cc.getInstance().getImg(),
                select,
                (Color c) -> {
                    double newblue = c.getRed()*.393 + c.getGreen()*.769 + c.getBlue()*.189;
                    double newred = c.getRed()*.349 + c.getGreen()*.686 + c.getBlue()*.168;
                    double newgreen = c.getRed()*.272 + c.getGreen()*.534 + c.getBlue()*.131;

                    int endred = (int) (newred > 1 ? 255 : newred*255);
                    int endgreen = (int) (newgreen > 1 ? 255 : newgreen*255);
                    int endblue = (int) (newblue > 1 ? 255 : newblue*255);

                    return Color.rgb(endred,endgreen,endblue);
                });
        Cc.getInstance().setImageAndRefreshView(image);


    }


    @FXML
    void mnuUndo(ActionEvent event) {

        Cc.getInstance().undo();

    }


    @FXML
    void mnuRedo(ActionEvent event) {
        Cc.getInstance().redo();
    }

    @FXML
    void mnuDelete(ActionEvent event) {

        if (Cc.getInstance().getImg() == null)
            return;

        //get selection area if there is one active
        Rectangle select = MiscCode.rectangleCopy(Cc.getInstance().getSelectionRectangle());
        Cc.getInstance().clearSelectionArea();
        //make sure that we set the image view first, so we can roll back and do other operations to it.
        Cc.getInstance().setImgView(this.imgView);
        Image image = Cc.transform(Cc.getInstance().getImg(),
                select,
                (Color c) -> {
                    return Color.rgb(255,255,255);
                });
        Cc.getInstance().setImageAndRefreshView(image);


    }

    @FXML
    void mnuSelectAll(ActionEvent event) {
        Cc.getInstance().clearSelectionArea();
    }

    @FXML
    void mnuUnselectAll(ActionEvent event) {
        Cc.getInstance().clearSelectionArea();
    }


    //##################################################################
    //INITIALIZE METHOD
    //see: http://docs.oracle.com/javafx/2/ui_controls/jfxpub-ui_controls.htm
    //##################################################################
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //mColor = Color.WHITE;
        //tgbCircle.setToggleGroup(mToggleGroup);
        //tgbSquare.setToggleGroup(mToggleGroup);
        //tgbFilter.setToggleGroup(mToggleGroup);
        //tgbCircle.setSelected(true);
//        cboSome.setValue("Darker");

        //load up blank image
        Cc.getInstance().setImgView(this.imgView);
        //File file = new File("C:\\dev\\java\\javasummer2016\\proImageShop\\src\\main\\resources\\images\\blank.png");
        File file = new File(System.getProperty("user.dir")+"\\src\\main\\resources\\images\\blank.png");
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Cc.getInstance().setImageAndRefreshView(SwingFXUtils.toFXImage(bufferedImage, null));
        } catch (IOException ex) {
            Logger.getLogger(ImageShopController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Cc.getInstance().setCurrentfile(file);
        //refresh image so I don't get buggy filter behavior
        snapshotImage();

        mToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (newValue == tgbCircle) {
                    penStyle = Pen.CIR;
                    System.out.println("Initialize: resources = "+resources );
                } else if (newValue == tgbSquare) {
                    penStyle = Pen.SQR;
                } else {
                    penStyle = Pen.CIR;
                }
            }
        });

        imgView.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {

                Cc.ToolbarButton toolbarButton = Cc.getInstance().getmToolbarButton();
                //clear previous rectangle selection if it exists
                Cc.getInstance().clearSelectionArea();

                //if rectangle select is active
                if (toolbarButton == Cc.ToolbarButton.R_SELECT) {

                    xPos = (int) me.getX();
                    yPos = (int) me.getY();
                    System.out.println("coordinates are " + xPos + ", " + yPos);
                    //set new selection box and encode the original x,y coordinates
                    Cc.getInstance().setSelectionRectangle(new Rectangle(xPos, yPos, 0, 0));
                    Cc.getInstance().setOriginalX(xPos);
                    Cc.getInstance().setOriginalY(yPos);
                    //make box black outlined and transparent
                    Rectangle selectbox = Cc.getInstance().getSelectionRectangle();
                    selectbox.setStroke(Color.BLACK);
                    selectbox.setFill(Color.TRANSPARENT);
                    ancRoot.getChildren().add(selectbox);
                    Cc.getInstance().set_rectangleExists(true);

                } //or if dropper, make sure we set color picker to the dropper color
                else if (toolbarButton == Cc.ToolbarButton.DROPPER) {
                    int xPosInt = (int) me.getX();
                    int yPosInt = (int) me.getY();
                    System.out.println("Dropper analyzed point "+xPosInt+", "+yPosInt);
                    Color color = Cc.getInstance().getImg().getPixelReader().getColor(xPosInt,yPosInt);
                    System.out.println("Color is "+color);
                    cpkColor.setValue(color);
                    mColor = color;
                } //or if dropper, call a flood function
                else if (toolbarButton == Cc.ToolbarButton.BUCKET) {
                    System.out.println("Requested bucket with color "+mColor+" on area "+xPos+", "+yPos);
                    System.out.println("Sorry, bucket is not operational right now");
                    Image image = Cc.bucketfill(Cc.getInstance().getImg(),
                            (int) me.getX(),
                            (int) me.getY(),
                            mColor);
                    Cc.getInstance().setImageAndRefreshView(image);
                }
                if (penStyle == Pen.FIL){
                    xPos = (int) me.getX();
                    yPos = (int) me.getY();
                }

                me.consume();
            }
        });


        imgView.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {

                Cc.ToolbarButton toolbarButton = Cc.getInstance().getmToolbarButton();

                //if we were on rectangular select
                if (toolbarButton == Cc.ToolbarButton.R_SELECT) {
                    snapshotImage();
                    ancRoot.getChildren().remove(Cc.getInstance().getSelectionRectangle());
                    System.out.println("Rectangle drawn: "+Cc.getInstance().getSelectionRectangle());
                } else if (toolbarButton == Cc.ToolbarButton.C_PEN
                        || toolbarButton == Cc.ToolbarButton.S_PEN
                        || toolbarButton == Cc.ToolbarButton.ERASE
                        || toolbarButton == Cc.ToolbarButton.PENCIL) {

                    System.out.println("mouse pressed! " + me.getSource());
                    snapshotImage();
                    ancRoot.getChildren().removeAll(removeShapes);
                    removeShapes.clear();

                } else if (penStyle == Pen.FIL){


                    wPos =  (int) me.getX() ;
                    hPos = (int) me.getY() ;

                    //default value
                   Image transformImage;

                    switch (mFilterStyle){
                        case DRK:
                            //make darker
                            transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                                    Cc.getInstance().getSelectionRectangle(),
                                    (x, y, c) -> (x > xPos && x < wPos)
                                            && (y > yPos && y < hPos) ?  c.deriveColor(0, 1, .5, 1): c
                            );
                            break;

                        case SAT:

                            //saturate
                            transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                                    Cc.getInstance().getSelectionRectangle(),
                                    (x, y, c) -> (x > xPos && x < wPos)
                                            && (y > yPos && y < hPos) ?  c.deriveColor(0, 1.0 / .1, 1.0, 1.0): c


                            );


                            break;

                        default:
                            //make darker
                            //make darker
                            transformImage = Cc.getInstance().transform(Cc.getInstance().getImg(),
                                    Cc.getInstance().getSelectionRectangle(),
                                    (x, y, c) -> (x > xPos && x < wPos)
                                            && (y > yPos && y < hPos) ?  c.deriveColor(0, 1, .5, 1): c
                            );
                            break;

                    }



                    Cc.getInstance().setImageAndRefreshView(transformImage);
                } else {
                    //do nothing right now

                }
                me.consume();
            }
        });


        imgView.addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {

                if (penStyle == Pen.FIL){
                    me.consume();
                    return;
                }

                // Line line = new Line(xPos, yPos, me.getX(), me.getY());

                xPos = me.getX();
                yPos = me.getY();
                Shape shape = new Circle(xPos, yPos, 10);
                Cc.ToolbarButton toolbarButton = Cc.getInstance().getmToolbarButton();

                //if we have a rectangle select action
                if (toolbarButton == Cc.ToolbarButton.R_SELECT) {
                    Rectangle rectangle = Cc.getInstance().getSelectionRectangle();
                    double oldX = Cc.getInstance().getOriginalX();
                    double oldY = Cc.getInstance().getOriginalY();
                    if (xPos >= oldX) {
                        rectangle.setWidth(xPos-oldX);
                    } else {
                        rectangle.setX(xPos);
                        rectangle.setWidth(oldX - xPos);
                    }
                    if (yPos >= oldY) {
                        rectangle.setHeight(yPos-oldY);
                    } else {
                        rectangle.setY(yPos);
                        rectangle.setHeight(oldY - yPos);
                    }
//                    rectangle.setHeight(yPos - rectangle.getY());
//                    rectangle.setWidth(xPos - rectangle.getX());

                } //if we have a circle pen or square pen selected
                //or we have eraser or pencil
                else if (toolbarButton == Cc.ToolbarButton.C_PEN ||
                        toolbarButton == Cc.ToolbarButton.S_PEN ||
                        toolbarButton == Cc.ToolbarButton.ERASE ||
                        toolbarButton == Cc.ToolbarButton.PENCIL) {

                    int nShape = 0;
                    //default value
                    final int PENCIL_RADIUS = 5;
                    switch (toolbarButton) {
                        case C_PEN:
                            shape = new Circle(xPos, yPos, penSize);
                            break;
                        case S_PEN:
                            shape = new Rectangle(xPos, yPos, penSize, penSize);
                            break;
                        case ERASE:
                            shape = new Rectangle(xPos,yPos,penSize,penSize);
                            break;
                        case PENCIL:
                            shape = new Circle(xPos,yPos, PENCIL_RADIUS);
                            break;

                        default:
                            shape = new Circle(xPos, yPos, penSize);
                            break;

                    }


                    // shape.setStroke(mColor);
                    if (toolbarButton == Cc.ToolbarButton.ERASE) {
                        shape.setFill(Color.WHITE);
                    } else {
                        shape.setFill(mColor);
                    }
                    ancRoot.getChildren().add(shape);
                    removeShapes.add(shape);
                }
                me.consume();

                //   Node shapeRemove =  ancRoot.getScene().lookup("789");
                //  ancRoot.getChildren().remove(shapeRemove);


            }
        });


        cpkColor.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mColor = cpkColor.getValue();
            }
        });

        sldSize.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                double temp  = (Double) newValue; //automatic unboxing
                penSize = (int) Math.round(temp);
            }
        });


//        cboSome.getItems().addAll(
//                "Darker",
//                "Saturate"
//
//        );


//        cboSome.valueProperty().addListener(new ChangeListener<String>() {
//            @Override
//            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//                switch (newValue){
//                    case "Saturate":
//                        mFilterStyle = FilterStyle.SAT;
//                        break;
//
//                    case "Darker":
//                        mFilterStyle = FilterStyle.DRK;
//                        break;
//
//                    default:
//                        mFilterStyle = FilterStyle.DRK;
//                        break;
//
//                }
//            }
//        });










    }//END INIT

    //helper method to snapshot image and then return it
    //fixes a bug where filtering rectangles don't draw properly on new images
    private void snapshotImage() {
        SnapshotParameters snapshotParameters = new SnapshotParameters();
        snapshotParameters.setViewport(new Rectangle2D(0, 0, imgView.getFitWidth(), imgView.getFitHeight()));
        Image snapshot = ancRoot.snapshot(snapshotParameters, null);
        Cc.getInstance().setImageAndRefreshView(snapshot);
    }


    //invert

//    Cc.getInstance().setSaturationLevel((int)sldSaturation.getValue());
//
//
//    int nLevel = Cc.getInstance().getSaturationLevel();
//    double dLevel = (100-nLevel)/100;
//
//    //saturation value
//    Image image = Cc.transform(Cc.getInstance().getImg(), (Color c, Double d) -> c.deriveColor(0, 1.0/ d, 1.0, 1.0), dLevel);
//    Cc.getInstance().getImgView().setImage(image);
//
//    Cc.getInstance().getSaturationStage().close();


}
