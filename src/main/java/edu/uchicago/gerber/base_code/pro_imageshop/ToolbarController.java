package edu.uchicago.gerber.base_code.pro_imageshop;

/**
 * toolbar controller
 */

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

public class ToolbarController {

    @FXML // fx:id="r_SelectButton"
    private Button r_SelectButton; // Value injected by FXMLLoader

    @FXML // fx:id="u_SelectButton"
    private Button u_SelectButton; // Value injected by FXMLLoader

    @FXML // fx:id="crop_SelectButton"
    private Button crop_SelectButton; // Value injected by FXMLLoader

    @FXML // fx:id="c_PenButton"
    private Button c_PenButton; // Value injected by FXMLLoader

    @FXML // fx:id="s_PenButton"
    private Button s_PenButton; // Value injected by FXMLLoader

    @FXML // fx:id="erase_Button"
    private Button eraser_Button; // Value injected by FXMLLoader

    @FXML // fx:id="pencil_Button"
    private Button pencil_Button; // Value injected by FXMLLoader

    @FXML // fx:id="dropper_Button"
    private Button dropper_Button; // Value injected by FXMLLoader

    @FXML // fx:id="bucket_Button"
    private Button bucket_Button; // Value injected by FXMLLoader

    @FXML
    void r_SelectButtonActivate(ActionEvent event) {
        Cc.getInstance().setmToolbarButton(Cc.ToolbarButton.R_SELECT);
        System.out.println(Cc.getInstance().getmToolbarButton());
    }

    @FXML
    void u_SelectButtonActivate(ActionEvent event) {
        Cc.getInstance().setmToolbarButton(Cc.ToolbarButton.U_SELECT);
        //clear out any selection box
        Cc.getInstance().clearSelectionArea();
        System.out.println(Cc.getInstance().getmToolbarButton());
    }

    @FXML
    void crop_SelectButtonActivate(ActionEvent event) {
        Cc.getInstance().setmToolbarButton(Cc.ToolbarButton.CROP);
        System.out.println(Cc.getInstance().getmToolbarButton());
    }

    @FXML
    void c_PenButtonActivate(ActionEvent event) {
        Cc.getInstance().setmToolbarButton(Cc.ToolbarButton.C_PEN);
        System.out.println(Cc.getInstance().getmToolbarButton());
    }

    @FXML
    void s_PenButtonActivate(ActionEvent event) {
        Cc.getInstance().setmToolbarButton(Cc.ToolbarButton.S_PEN);
        System.out.println(Cc.getInstance().getmToolbarButton());
    }

    @FXML
    void erase_ButtonActivate(ActionEvent event) {
        Cc.getInstance().setmToolbarButton(Cc.ToolbarButton.ERASE);
        System.out.println(Cc.getInstance().getmToolbarButton());
    }

    @FXML
    void pencil_ButtonActivate(ActionEvent event) {
        Cc.getInstance().setmToolbarButton(Cc.ToolbarButton.PENCIL);
        System.out.println(Cc.getInstance().getmToolbarButton());
    }

    @FXML
    void dropper_ButtonActivate(ActionEvent event) {
        Cc.getInstance().setmToolbarButton(Cc.ToolbarButton.DROPPER);
        System.out.println(Cc.getInstance().getmToolbarButton());
    }

    @FXML
    void bucket_ButtonActivate(ActionEvent event) {
        Cc.getInstance().setmToolbarButton(Cc.ToolbarButton.BUCKET);
        System.out.println(Cc.getInstance().getmToolbarButton());
    }

//
//    @FXML
//    void okButtonAction(ActionEvent event) {
//
//        //saturation value .9 is little  and .1 is a lot. So we need to invert the value from the slider
//        double dLevel = 1 - sldSaturation.getValue();
//
//
//        Image image = Cc.transform(Cc.getInstance().getImg(), (Color c, Double d) -> c.deriveColor(0, 1.0 / d, 1.0, 1.0), dLevel);
//        Cc.getInstance().setImageAndRefreshView(image);
//       // Cc.getInstance().getImgView().setImage(image);
//        Cc.getInstance().getSaturationStage().close();
//
//
//    }




}
