package edu.uchicago.gerber.base_code.pro_imageshop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kaushal on 7/24/2016.
 */
public class Test {

    public static void main(String[] args) {

        List<Integer> testlist = new ArrayList<>();
        System.out.println("new array size is "+testlist.size());
        testlist.add(1);
        testlist.remove(0);
        if (testlist.size() == 0) {
            System.out.println("hi");
        }

        for (int j = 1; j < 10; j++) {
            testlist.add(j);
        }

        System.out.println("length is "+testlist.size());
        System.out.println("first element is "+testlist.get(0));
        testlist.remove(0);
        System.out.println("first element is now "+testlist.get(0));

    }

}
