package edu.uchicago.gerber.base_code.pro_imageshop;

import javafx.scene.shape.Rectangle;

/**
 * Created by Kaushal on 7/27/2016.
 */
public class MiscCode {

    public static Rectangle rectangleCopy (Rectangle rect) {
        if (rect != null) {
            return new Rectangle(
                    rect.getX(),
                    rect.getY(),
                    rect.getWidth(),
                    rect.getHeight());
        } else {
            return null;
        }
    }

}
