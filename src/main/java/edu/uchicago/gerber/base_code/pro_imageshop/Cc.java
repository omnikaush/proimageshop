package edu.uchicago.gerber.base_code.pro_imageshop;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

public class Cc {

    private static Cc stateManager;




    private Stage mMainStage, mSaturationStage, mToolbarStage;
    private ImageView imgView; // Value injected by FXMLLoader
    private Image img, imgUndo;
    private int mPointTemp;
    //the selection box and the original x and y
    private Rectangle selectionRectangle;
    private double originalX;
    private double originalY;
    //the index of the selection box in the children area
    private int selectIndex;
    private boolean rectangle_Exists;
   // private boolean bFirstUndo = true;
    final int MAX_UNDO_SIZE = 15;
    private File currentfile, lastfile;

    private static List<Image> backImages;

    //for undo and redo
    private LinkedList<Image> redoImages;


    /**
     * Create private constructor
     */
    private Cc(){

    }
    /**
     * Create a static method to get instance.
     */
    public static Cc getInstance(){
        if(stateManager == null){
            stateManager = new Cc();
            backImages = new LinkedList<>();
        }
        return stateManager;
    }

    //used to see what is marked on ToolbarButton
    public enum ToolbarButton {
        R_SELECT, C_SELECT, U_SELECT, CROP,
        S_PEN, C_PEN, ERASE, PENCIL,
        DROPPER, BUCKET;
    }

    private ToolbarButton mToolbarButton = ToolbarButton.U_SELECT;


    //from Horstmann
    public static Image transform(Image in, Rectangle rect, UnaryOperator<Color> f) {
        int x1, x2, y1, y2; //coordinates of the box we are drawing in

        int width = (int) in.getWidth();
        int height = (int) in.getHeight();
        if (rect == null || rect.getHeight() == 0 && rect.getWidth() == 0) {
            x1 = 0; y1 = 0; x2 = width; y2 = height;
        } else {
            x1 = (int) rect.getX(); y1 = (int) rect.getY();
            x2 = ((int) rect.getWidth()) + x1;
            y2 = ((int) rect.getHeight()) + y1;
        }
        //apply transformation
        WritableImage out = new WritableImage(
                width, height);
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                if (x >= x1 && x <= x2 && y >= y1 && y <= y2) { //if we fall into bounds of selection area
                    out.getPixelWriter().setColor(x, y,
                            f.apply(in.getPixelReader().getColor(x, y)));
                } else {
                    out.getPixelWriter().setColor(x,y,in.getPixelReader().getColor(x,y));
                }
        return out;
    }

    public static <T> Image transform(Image in, Rectangle rect, BiFunction<Color, T, Color> f, T arg) {
        int x1, x2, y1, y2; //coordinates of the box we are drawing in

        int width = (int) in.getWidth();
        int height = (int) in.getHeight();
        if (rect == null || rect.getHeight() == 0 && rect.getWidth() == 0) {
            x1 = 0; y1 = 0; x2 = width; y2 = height;
        } else {
            x1 = (int) rect.getX(); y1 = (int) rect.getY();
            x2 = ((int) rect.getWidth()) + x1;
            y2 = ((int) rect.getHeight()) + y1;
        }
        //apply transformation
        WritableImage out = new WritableImage(
                width, height);
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                if (x >= x1 && x <= x2 && y >= y1 && y <= y2) {
                    out.getPixelWriter().setColor(x, y,
                            f.apply(in.getPixelReader().getColor(x, y), arg));
                } else {
                    out.getPixelWriter().setColor(x,y,in.getPixelReader().getColor(x,y));
                }
        return out;
    }

    //working on implementation...
    public static Image bucketfill(Image in, int xPos, int yPos, Color color) {
        return in;
    };

    private static void flood(Image img, int xPos, int yPos, Color srcColor, Color tgtColor) {

    }

    public static Image transform(Image in, Rectangle rect, ColorTransformer f) {
        int width = (int) in.getWidth();
        int height = (int) in.getHeight();
        WritableImage out = new WritableImage(
                width, height);
        for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
                out.getPixelWriter().setColor(x, y,
                        f.apply(x, y, in.getPixelReader().getColor(x, y)));
        return out;
    }

    public double getOriginalX() {
        return originalX;
    }

    public void setOriginalX(double originalX) {
        this.originalX = originalX;
    }

    public double getOriginalY() {
        return originalY;
    }

    public void setOriginalY(double originalY) {
        this.originalY = originalY;
    }

    public boolean rectangle_Exists() {
        return rectangle_Exists;
    }

    public void set_rectangleExists(boolean test) {
        this.rectangle_Exists = test;
    }

    public int getSelectIndex() {
        return selectIndex;
    }

    public void setSelectIndex(int selectIndex) {
        this.selectIndex = selectIndex;
    }

    public Rectangle getSelectionRectangle() {
        return selectionRectangle;
    }

    public void setSelectionRectangle(Rectangle selectionRectangle) {
        this.selectionRectangle = selectionRectangle;
    }

    public ToolbarButton getmToolbarButton() { return mToolbarButton; }

    public void setmToolbarButton(ToolbarButton mToolbarButton) { this.mToolbarButton = mToolbarButton; }

    public Stage getMainStage() {
        return mMainStage;
    }

    public void setMainStage(Stage mMainStage) {
        this.mMainStage = mMainStage;
    }

    public Stage getSaturationStage() {
        return mSaturationStage;
    }

    public void setSaturationStage(Stage mSaturationStage) {
        this.mSaturationStage = mSaturationStage;
    }

    public Stage getmToolbarStage() { return mToolbarStage; }

    public void setmToolbarStage(Stage mToolbarStage) { this.mToolbarStage = mToolbarStage; }

    public ImageView getImgView() {
        return imgView;
    }

    public void setImgView(ImageView imgView) {
        this.imgView = imgView;
    }

    public Image getImg() {
        return img;
    }

    public File getCurrentfile() {
        return currentfile;
    }

    public void setCurrentfile(File currentfile) {
        this.currentfile = currentfile;
    }

    public File getLastfile() {
        return lastfile;
    }

    public void setLastfile(File lastfile) {
        this.lastfile = lastfile;
    }

    public File lastfile() {
        return lastfile;
    }

    public void undo(){

//        if (imgUndo != null){
//            this.img = imgUndo;
//            imgView.setImage(img);
//        }
        clearSelectionArea();
        if (backImages != null) {
            int undoListSize = backImages.size();
            //if the list size is over zero, let the user undo
            if (undoListSize > 0) {
                //add in entry to redo list so you can undo your "undo"
                redoImages.add(this.img);
                //now change image to undo image
                this.img = backImages.get(undoListSize - 1);
                imgView.setImage(img);
                backImages.remove(undoListSize - 1);
            }

            //System.out.println("After undo method, undo list size is "+backImages.size());
        }
    }


    public void redo(){
        if (redoImages != null){
            if (redoImages.size() > 0) {
                //add the image back to undo list
                backImages.add(this.img);
                //now redo image
                this.img = redoImages.get(redoImages.size()-1);
                imgView.setImage(img);
                redoImages.remove(redoImages.size()-1);
            }
        }
   }



    public void setImageAndRefreshView(Image img){
        //undo image
        addUndoImageAndClearRedo();

        this.img = img;
        imgView.setImage(img);
    }


    private void addUndoImageAndClearRedo() {
        //if list is null, create a new list
        if (backImages == null) { backImages = new LinkedList<>(); }
        //if the list is equal to or greater than the max size, remove the first item
        if (backImages.size() >= MAX_UNDO_SIZE) {
            backImages.remove(0);
        }
        backImages.add(this.img);

        //clear out redo images because we are starting a new "branch" so to speak
        redoImages = new LinkedList<>();
    }

    //for clearing a selection box that was drawn
    public void clearSelectionArea () {
        //is there a selection box?
        if (rectangle_Exists) {
            //clear rectangle by going back one image
            int undoListSize = backImages.size();
            this.img = backImages.get(undoListSize - 1);
            imgView.setImage(img);
            backImages.remove(undoListSize - 1);
            //make rectangle null in Cc singleton
            selectionRectangle = null;
            //turn boolean to false
            rectangle_Exists = false;
        }
    }


    public void close(){
        //clear out undo and redo lists
        clearSelectionArea();
        redoImages.clear();
        backImages.clear();
        imgView.setImage(null);
    }
}
